package com.homework.first.rest.restcontroller;

import com.homework.first.rest.response.Message;
import com.homework.first.service.impl.BookServiceImpl;
import com.homework.first.repository.model.BookModel;
import com.homework.first.rest.request.BookRequestModel;
import com.homework.first.rest.response.BaseApiResponse;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping("/api/v1")
public class BookRestController {

    private BookServiceImpl bookService;

    @Autowired
    public void setArticleService(BookServiceImpl articleService) {
        this.bookService = articleService;
    }

    @PostMapping("/books/post")
    public ResponseEntity<BaseApiResponse<BookRequestModel>> insert(
            @RequestBody BookRequestModel books) {

        BaseApiResponse<BookRequestModel> response = new BaseApiResponse<>();

        // Validate ->

        ModelMapper mapper = new ModelMapper();
        BookModel bookModel = mapper.map(books, BookModel.class);
        BookModel result = bookService.insert(bookModel);

        BookRequestModel result2 = mapper.map(result, BookRequestModel.class);

        response.setMessage("You have added book successfully");
        response.setData(result2);
        response.setStatus(HttpStatus.OK);
        response.setTime(new Timestamp(System.currentTimeMillis()));

        return ResponseEntity.ok(response);

    }

    @GetMapping("/books/select")
    public ResponseEntity<BaseApiResponse<List<BookRequestModel>>> select() {

        ModelMapper mapper = new ModelMapper();
        BaseApiResponse<List<BookRequestModel>> response =
                new BaseApiResponse<>();

        List<BookModel> bookModelList = bookService.select();
        List<BookRequestModel> bookRequestModels = new ArrayList<>();

        for (BookModel bookModel : bookModelList) {
            bookRequestModels.add(mapper.map(bookModel, BookRequestModel.class));
        }

        response.setData(bookRequestModels);
        response.setMessage("success");
        response.setStatus(HttpStatus.OK);
        response.setTime(new Timestamp(System.currentTimeMillis()));

        return ResponseEntity.ok(response);
    }
    @DeleteMapping("/books/delete/{id}")
    public ResponseEntity<Message> delete(@PathVariable int id){
        Message message = new Message();
        if(!"".equals(id)){
            bookService.deleteBook(id);
            message.setMessage("You delete successfully");
            message.setStatus(HttpStatus.OK);
            return ResponseEntity.ok(message);
        }else{
            message.setMessage("You delete fail !");
            message.setStatus(HttpStatus.OK);
            return ResponseEntity.ok(message);
        }
    }

    @PutMapping("/books/update/{id}")
    public ResponseEntity<Message> Update(@PathVariable int id, @RequestBody BookRequestModel bookRequestModel){
        Message message = new Message();
        ModelMapper modelMapper = new ModelMapper();
        BookModel bookModel = modelMapper.map(bookRequestModel,BookModel.class);
        if(!("".equals(id)||"".equals(bookModel))) {
            bookService.updateBookById(id, bookModel);
            message.setMessage("You have Updated successfully");
            message.setStatus(HttpStatus.OK);
            return ResponseEntity.ok(message);
        }else{
            message.setMessage("You have Invalided to Update!");
            message.setStatus(HttpStatus.BAD_REQUEST);
            return ResponseEntity.ok(message);
        }
    }

    @GetMapping("/books/search")
    public ResponseEntity<BaseApiResponse<List<BookModel>>> filterByTitle(@RequestParam(required = false) String title){

        ModelMapper modelMapper = new ModelMapper();
        BaseApiResponse<List<BookModel>> response = new BaseApiResponse<>();

        List<BookModel> resultQuery = bookService.filterByTitle(title);

        response.setMessage("You have found by book title");
        response.setData(resultQuery);
        response.setStatus(HttpStatus.OK);
        response.setTime(new Timestamp(System.currentTimeMillis()));
        return ResponseEntity.ok(response);
    }

    @GetMapping("/books/find")
    public ResponseEntity<BaseApiResponse<List<BookModel>>> findById(@RequestParam(required = false) int id) {

        ModelMapper modelMapper = new ModelMapper();
        BaseApiResponse<List<BookModel>> response = new BaseApiResponse<>();

        List<BookModel> resultQuery = bookService.findById(id);

        response.setMessage("You have found by book title");
        response.setData(resultQuery);
        response.setStatus(HttpStatus.OK);
        response.setTime(new Timestamp(System.currentTimeMillis()));
        return ResponseEntity.ok(response);
    }
}
