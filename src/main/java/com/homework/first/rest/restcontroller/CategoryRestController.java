package com.homework.first.rest.restcontroller;

import com.homework.first.repository.model.CategoryModel;
import com.homework.first.rest.request.CategoryRequestModel;
import com.homework.first.rest.response.BaseApiResponse;
import com.homework.first.rest.response.Message;
import com.homework.first.service.impl.CategoryServiceImp;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.sql.Timestamp;
import java.util.List;
@RestController
public class CategoryRestController {
    private CategoryServiceImp categoryService;
    @Autowired
    public void setCategoryService(CategoryServiceImp categoryService) {
        this.categoryService = categoryService;
    }

    @PostMapping("/category")
    public ResponseEntity<BaseApiResponse<CategoryRequestModel>> insert(@RequestBody CategoryRequestModel categoryRequestModel){


        BaseApiResponse<CategoryRequestModel> responseAIP = new BaseApiResponse<>();
        ModelMapper modelMapper = new ModelMapper();

        CategoryModel categoryModel = modelMapper.map(categoryRequestModel,CategoryModel.class);
        CategoryModel result = categoryService.insert(categoryModel);

        CategoryRequestModel  reponse = modelMapper.map(result,CategoryRequestModel.class);

        responseAIP.setMessage("You have add book successfully");
        responseAIP.setData(reponse);
        responseAIP.setStatus(HttpStatus.OK);
        responseAIP.setTime(new Timestamp(System.currentTimeMillis()));
        return ResponseEntity.ok(responseAIP);
    }
    @GetMapping("/category")
    public ResponseEntity<BaseApiResponse<List<CategoryModel>>> select(){
        ModelMapper modelMapper = new ModelMapper();
        BaseApiResponse<List<CategoryModel>> response = new BaseApiResponse<>();

        List<CategoryModel> resultQuery = categoryService.select();

        response.setMessage("You have found all Category successfully");
        response.setData(resultQuery);
        response.setStatus(HttpStatus.OK);
        response.setTime(new Timestamp(System.currentTimeMillis()));
        return ResponseEntity.ok(response);
    }
    @DeleteMapping("/category/{id}")
    public ResponseEntity<Message>  deleteById(@PathVariable int id){

        Message message = new Message();

        if(!"".equals(id)){
            categoryService.DeletedCategoryById(id);
            message.setMessage("You deleted successfully");
            message.setStatus(HttpStatus.OK);
            return ResponseEntity.ok(message);
        }else{
            message.setMessage("You false to delete!");
            message.setStatus(HttpStatus.BAD_REQUEST);
            return ResponseEntity.ok(message);
        }

    }
    @PutMapping("/category/{id}")
    public ResponseEntity<Message> Update(@PathVariable int id, @RequestBody CategoryRequestModel categoryRequestModel){
        Message message = new Message();
        ModelMapper modelMapper = new ModelMapper();
        CategoryModel categoryModel = modelMapper.map(categoryRequestModel,CategoryModel.class);
        if(!("".equals(id)||"".equals(categoryModel))){
            categoryService.UpdateCategory(id,categoryModel);
            message.setMessage("You Update successfully");
            message.setStatus(HttpStatus.OK);
            return ResponseEntity.ok(message);
        }else{
            message.setMessage("You Invalid to Update!");
            message.setStatus(HttpStatus.BAD_REQUEST);
            return ResponseEntity.ok(message);
        }
    }

}
