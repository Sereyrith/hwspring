package com.homework.first.rest.response;

import org.springframework.http.HttpStatus;

public class Message {
    private String Message;
    private HttpStatus status;

    public Message() {
    }

    public Message(String message, HttpStatus status) {
        Message = message;
        this.status = status;
    }

    public String getMessage() {
        return Message;
    }

    public void setMessage(String message) {
        Message = message;
    }

    public HttpStatus getStatus() {
        return status;
    }

    public void setStatus(HttpStatus status) {
        this.status = status;
    }

    @Override
    public String toString() {
        return "Message{" +
                "Message='" + Message + '\'' +
                ", status=" + status +
                '}';
    }
}
