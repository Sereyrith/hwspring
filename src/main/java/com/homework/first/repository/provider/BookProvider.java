package com.homework.first.repository.provider;

import com.homework.first.repository.model.BookModel;
import org.apache.ibatis.jdbc.SQL;

public class BookProvider {

    public String select() {
        return new SQL(){{
            SELECT("*");
            FROM("tbl_book");
        }}.toString();
    }

public String selectCategoryById(int id){
    return new SQL(){{
        SELECT("*");
        FROM("tbl_book b");
        INNER_JOIN("tbl_categories c ON b.category_id = c.category_id");
        WHERE("c.category_id = #{id}");
    }}.toString();
}


    public String deleteBookById(int id){
        return new SQL(){{
            DELETE_FROM("tbl_books");
            WHERE("id = #{id}");
        }}.toString();
    }


    public String updateBookById(int id, BookModel book){
        return new SQL(){{
            UPDATE("tbl_book SET title = #{book.title} , author = #{book.author}, " +
                    "thumbnail = #{book.thumbnail}, description = #{book.description}, " +
                    "category_id = #{book.category.id}");
            WHERE("id = '"+id+"'");
        }}.toString();
    }


    public String selectFilterByBookTitle(String title){
        return new SQL(){{
            SELECT("*");
            FROM("tbl_book");
            if(title !=null || !title.equals("")){
                WHERE("title like '%"+title+"%'");
            }else{
                WHERE();
            }
        }}.toString();
    }

    public String findById(int id){
        return new SQL(){{
            SELECT("*");
            FROM("tbl_book");
            WHERE("id = #{id}");
        }}.toString();
    }


}
