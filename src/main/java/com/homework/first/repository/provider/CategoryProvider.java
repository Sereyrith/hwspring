package com.homework.first.repository.provider;

import com.homework.first.repository.model.CategoryModel;
import org.apache.ibatis.jdbc.SQL;

public class CategoryProvider {
    public String InsertCategory(){
        return  new SQL(){{
            INSERT_INTO("tbl_categories");
            VALUES("title","#{title}");
        }}.toString();
    }
    public String SelectCategory(){
        return  new SQL(){{
            SELECT("*");
            FROM("tbl_categories");
        }}.toString();
    }
    public String DeleteCategory(int id){
        return new SQL(){{
            DELETE_FROM("tbl_categories");
            WHERE("category_id = #{id}");
        }}.toString();
    }
    public String UpdateCategory(int id, CategoryModel categoryModel){
        return new SQL(){{
            UPDATE("tbl_categories SET title = #{categoryModel.title}  Where category_id = '"+id+"'");
        }}.toString();
    }
}
