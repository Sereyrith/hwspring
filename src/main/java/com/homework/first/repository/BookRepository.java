package com.homework.first.repository;

import com.homework.first.repository.model.BookModel;
import com.homework.first.repository.model.CategoryModel;
import com.homework.first.repository.provider.BookProvider;
import org.apache.ibatis.annotations.DeleteProvider;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.SelectProvider;
import org.apache.ibatis.annotations.UpdateProvider;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface BookRepository {

    @Insert("INSERT INTO tbl-book (title, author, description, thumbnail, category_id) " +
            "VALUES (#{title}, #{author}, #{description}, #{thumbnail}, #{categoryId})")
    boolean insert(BookModel article);

    // Not using provider
    //@Select("SELECT * FROM articles")
    // Using provider
    @SelectProvider(value = BookProvider.class, method = "select")
    List<BookModel> select();

    @SelectProvider(type = BookProvider.class,method = "selectCategoryById")
    List<CategoryModel> selectCategoryById(int id);


    @DeleteProvider(type = BookProvider.class, method = "deleteBookById")
    int deleteBook(int id);

    @UpdateProvider(type = BookProvider.class,method = "updateBookById")
    int updateBookById(int id , BookModel bookModel);

    @SelectProvider(type = BookProvider.class,method = "selectFilterByBookTitle")
    List<BookModel> filterByTitle(String title);

    @SelectProvider(type = BookProvider.class,method = "findById")
    List<BookModel> findById(int id);

}
