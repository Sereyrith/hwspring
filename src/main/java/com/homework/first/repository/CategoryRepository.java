package com.homework.first.repository;

import com.homework.first.repository.model.CategoryModel;
import com.homework.first.repository.provider.CategoryProvider;
import org.apache.ibatis.annotations.*;

import java.util.List;

public interface CategoryRepository {
    @InsertProvider(type = CategoryProvider.class,method = "InsertCategory")
    boolean insert(CategoryModel categoryModel);
    @SelectProvider(type = CategoryProvider.class,method = "SelectCategory")
    @Results({
            @Result(property = "id",column = "category_id")
    })
    List<CategoryModel> select();

    @DeleteProvider(type = CategoryProvider.class,method = "DeleteCategory")
    int DeletedCategoryById(int id);

    @UpdateProvider(type = CategoryProvider.class,method = "UpdateCategory")
    int UpdateCategory(int id,CategoryModel categoryModel);
}
