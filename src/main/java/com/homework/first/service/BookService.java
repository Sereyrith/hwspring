package com.homework.first.service;

import com.homework.first.repository.model.BookModel;

import java.util.List;

public interface BookService {

    BookModel insert(BookModel article);
    List<BookModel> select();
    int deleteBook(int id);
    int updateBookById(int id,BookModel bookModel);
    List<BookModel> filterByTitle(String title);
    List<BookModel> findById(int id);
}
