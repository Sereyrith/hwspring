package com.homework.first.service.impl;

import com.homework.first.repository.CategoryRepository;
import com.homework.first.repository.model.CategoryModel;
import com.homework.first.service.CategoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
@Service
public class CategoryServiceImp implements CategoryService {
    private CategoryRepository categoryRepository ;

    @Autowired
    public void setCategoryRepository(CategoryRepository categoryRepository) {
        this.categoryRepository = categoryRepository;
    }


    @Override
    public CategoryModel insert(CategoryModel categoryModel) {
        boolean isInserted  = categoryRepository.insert(categoryModel);
        if(isInserted){
            return categoryModel;
        }else{
            return null;
        }
    }

    @Override
    public List<CategoryModel> select() {
        return categoryRepository.select();
    }

    @Override
    public int DeletedCategoryById(int id) {
        return categoryRepository.DeletedCategoryById(id);
    }

    @Override
    public int UpdateCategory(int id,CategoryModel categoryModel) {
        return categoryRepository.UpdateCategory(id,categoryModel);
    }
}
