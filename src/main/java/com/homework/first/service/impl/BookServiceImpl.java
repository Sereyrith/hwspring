package com.homework.first.service.impl;

import com.homework.first.repository.BookRepository;
import com.homework.first.service.BookService;
import com.homework.first.repository.model.BookModel;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class BookServiceImpl implements BookService {

    private BookRepository bookRepository;

    @Autowired
    public void setBookRepository(BookRepository bookRepository) {
        this.bookRepository = bookRepository;
    }

    @Override
    public BookModel insert(BookModel book) {
        boolean isInserted = bookRepository.insert(book);
        if (isInserted)
            return book;
        else
            return null;
    }

    @Override
    public List<BookModel> select() {
        return bookRepository.select();
    }

    @Override
    public int deleteBook(int id) {
        return bookRepository.deleteBook(id);
    }

    @Override
    public int updateBookById(int id, BookModel bookModel) {
        return bookRepository.updateBookById(id,bookModel);
    }

    @Override
    public List<BookModel> filterByTitle(String title) {
        return bookRepository.filterByTitle(title);
    }

    @Override
    public List<BookModel> findById(int id) {
        return bookRepository.findById(id);
    }
}
