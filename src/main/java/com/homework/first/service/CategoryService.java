package com.homework.first.service;

import com.homework.first.repository.model.CategoryModel;

import java.util.List;

public interface CategoryService {
    CategoryModel insert(CategoryModel categoryModel);
    List<CategoryModel> select();
    int DeletedCategoryById(int id);
    int UpdateCategory(int id,CategoryModel categoryModel);
}
